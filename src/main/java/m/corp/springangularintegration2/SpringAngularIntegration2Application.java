package m.corp.springangularintegration2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAngularIntegration2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringAngularIntegration2Application.class, args);
	}
}
